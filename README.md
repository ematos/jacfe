# J.A.C.F.E (Just Another Cache Failover Example)

Cache faileover example written in Python

## Dependencies

1. Docker
1. Docker Compose

Or

1. Python 3.6
1. Redis
1. MySQL

## Installation

As long as you have [`Docker`](https://www.docker.com/) and [`Docker Compose`](https://docs.docker.com/compose/)
installed, you're good to go.

If you don't want to rely on `Docker`/`Docker Compose` to run the project, make sure you created and activated a
`virtualenv` with python3.6, then `pip install -r requirements-dev.txt`.

**Optional tooling:**

* [`virtualenvwrapper`](http://virtualenvwrapper.readthedocs.io/en/latest/index.html): It may be useful to simplify virtualenv creation.
* [`pyenv`](https://github.com/pyenv/pyenv): Because installing different Python versions is painful sometimes.

## Running

**Locally**

In case you chose to use `Docker Compose`, then

```sh
docker-compose up
```

Otherwhise you may run directly with Python, which starts faster than docker-compose (good for debugging purpose).

```sh
python -m src
```

**Production**

In production you'd better use Docker. Any future machine migration will be as simple as running this project's image
elsewhere.

```sh
docker build . -t jacfe
docker run -e JACFE_PORT="<PORT>" -e JACFE_DATABASE_URL="DATABASE_URL" -e JACFE_CACHE_REDIS_URL="REDIS_URL" --expose 8080:8080 jacfe
```

Or run directly with Gunicorn (you'll need to setup a virtualenv with Python 3.6):

```sh
pip install -r requirements.txt
JACFE_PORT="<PORT>" JACFE_DATABASE_URL="DATABASE_URL" JACFE_CACHE_REDIS_URL="REDIS_URL" gunicorn -c gunicorn_config.py src.__main__:app
```

## Environment Variables

* `JACFE_PORT` (defaults to 8080): The port which the app will run
* `JACFE_DATABASE_URL` (defaults to `sqlite:///`): The database URL. Eg.: `mysql://user:password@host:port/database`
* `JACFE_CACHE_REDIS_URL` (defaults to `redis://localhost`)

## Endpoints

All endpoints responses are valid JSON

### GET /customers

Returns all customers

**Example request**:

```
curl http://localhost:8080/customers
```

**Example response**:

```js
[{"id": 1, "name": "Spam", "age": 31}]
```

### POST /customers

Saves new customers

**Example request**:

```sh
curl -H "Content-Type: application/json" -X POST -d '[{"name":"Fish","age":31}]' http://localhost:8080/customers
```

**Example response**:

The status code is 201 (Created).

```js
{"status": "Customers successfully created"}
```

If no new customers are saved, then the status code is 400, and the response text is:

```js
{"status": "No donuts for you"}
```

### DELETE /customers

Deletes all customers

**Example request**:

```sh
curl -X DELETE http://localhost:8080/customers
```

**Example response**:

The status code is 200 (Ok).

```js
{"status": "Customers successfully deleted"}
```

## Testing

Unfortunately you'll need to setup this project with Python to run the tests.  
Docker Compose still can't be used for this purpose. At the very least this project is being continuousEly
tested on [Bitbucket Pipelines](https://bitbucket.org/ematos/jacfe/addon/pipelines/home#!/).

```sh
python -m unittest discover
```

You can also lint it with [`flake8`](http://flake8.pycqa.org/en/latest/):

```
flake8 .
```

To typecheck, just run

```
mypy --ignore-missing-imports -p src -p tests -p scripts
```

**Optional tooling:**

* [`pytest`](https://docs.pytest.org/en/latest/): More beautiful test output.

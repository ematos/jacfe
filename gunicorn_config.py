from os import environ

bind = f'0.0.0.0:{environ.get("JACFE_PORT", 8080)}'
workers = 1
worker_class = 'gevent'
timeout = 30

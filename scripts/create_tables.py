from time import sleep

from sqlalchemy.exc import DBAPIError

from src.logger import logger
from src.models import db


while True:
    try:
        db.create_all()
        logger.info('Tables successfully created')
        break
    except DBAPIError:
        logger.info('Could not create tables. Trying again in 1 second...')
        sleep(1)

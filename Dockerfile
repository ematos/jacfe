FROM python:3.6

COPY . /opt/app
WORKDIR /opt/app

RUN pip install -r requirements.txt

CMD ["gunicorn", "-c", "gunicorn_config.py", "src.__main__:app"]

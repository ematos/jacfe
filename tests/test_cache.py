from unittest import TestCase
from unittest.mock import patch, call

from redis.exceptions import ConnectionError

from src.cache import try_get, try_set, try_clear


class CacheTest(TestCase):
    def setUp(self) -> None:
        self.cache = patch('src.cache.cache').start()

    def tearDown(self) -> None:
        patch.stopall()

    def test_try_get_returns_cache_result_if_operation_succeeds(self) -> None:
        self.cache.get.return_value = 'Spam'

        self.assertEqual('Spam', try_get('some_key'))
        self.assertEqual([call('some_key')], self.cache.get.call_args_list)

    def test_try_get_returns_none_if_operation_fails_due_to_connection_error(self) -> None:  # noqa: E501
        self.cache.get.side_effect = ConnectionError

        self.assertEqual(None, try_get('some_key'))
        self.assertEqual([call('some_key')], self.cache.get.call_args_list)

    def test_try_get_raises_if_operation_fails_due_to_non_connection_error(self) -> None:  # noqa: E501
        self.cache.get.side_effect = Exception

        self.assertRaises(Exception, try_get, 'some_key')
        self.assertEqual([call('some_key')], self.cache.get.call_args_list)

    def test_try_set_doesnt_raise_if_operation_fails_due_to_connection_error(self) -> None:  # noqa: E501
        self.cache.set.side_effect = ConnectionError

        try_set('some_key', 'some_value')
        self.assertEqual([call('some_key', 'some_value')],
                         self.cache.set.call_args_list)

    def test_try_set_raises_if_operation_fails_due_to_non_connection_error(self) -> None:  # noqa: E501
        self.cache.set.side_effect = Exception

        self.assertRaises(Exception, try_set, 'some_key', 'some_value')
        self.assertEqual([call('some_key', 'some_value')],
                         self.cache.set.call_args_list)

    def test_try_clear_dont_raise_if_operation_fails_due_to_connection_error(self) -> None:  # noqa: E501
        self.cache.clear.side_effect = ConnectionError

        try_clear()
        self.assertEqual([call()], self.cache.clear.call_args_list)

    def test_try_clear_raises_if_operation_fails_due_to_non_connection_error(self) -> None:  # noqa: E501
        self.cache.clear.side_effect = Exception

        self.assertRaises(Exception, try_clear)
        self.assertEqual([call()], self.cache.clear.call_args_list)

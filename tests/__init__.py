from flask import Flask
from flask_testing import TestCase

from src.cache import cache
from src.models import db
from src.routes import app


class BaseTest(TestCase):
    def create_app(self) -> Flask:
        app.config['TESTING'] = True
        cache.init_app(app, config={'CACHE_TYPE': 'simple'})
        return app

    def __call__(self, *args, **kwargs) -> None:
        db.create_all()

        super().__call__(*args, **kwargs)

        db.drop_all()
        cache.clear()

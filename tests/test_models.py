from src.models import Customer
from . import BaseTest


class CustomerTest(BaseTest):
    def test_repr(self) -> None:
        customer = Customer('Spam', 19)
        self.assertEqual('<Customer (name="Spam", age=19)>', str(customer))

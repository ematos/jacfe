import json
from http import HTTPStatus
from typing import Union
from unittest.mock import ANY, patch, MagicMock

from flask import url_for, Response

from src.cache import cache
from src.models import db, Customer
from . import BaseTest


class GetCustomerTest(BaseTest):
    def test_returns_all_customers(self) -> None:
        db.session.add(Customer(name='Spam', age=21))
        db.session.add(Customer(name='Fish', age=43))
        db.session.commit()

        resp = self.client.get(url_for('show_customers'))

        expected = [
            {'id': ANY, 'age': 21, 'name': 'Spam'},
            {'id': ANY, 'age': 43, 'name': 'Fish'},
        ]
        self.assertEqual(expected, resp.json)

    def test_returns_empty_json_if_there_are_no_customers(self) -> None:
        resp = self.client.get(url_for('show_customers'))

        self.assertEqual(HTTPStatus.OK, resp.status_code)
        self.assertEqual([], resp.json)

    @patch('src.routes.Customer')
    def test_doesnt_hit_database_if_cached_result_is_not_none(self, Customer: MagicMock) -> None:  # noqa: E501
        cache.set('customers', [])
        self.client.get(url_for('show_customers'))

        self.assertEqual([], Customer.query.all.call_args_list)

    def test_returns_cached_customers(self) -> None:
        cached_customers = [{'id': 1, 'name': 'Spam', 'age': 99}]
        cache.set('customers', cached_customers)

        resp = self.client.get(url_for('show_customers'))

        self.assertEqual(HTTPStatus.OK, resp.status_code)
        self.assertEqual(cached_customers, resp.json)

    def test_cache_customers(self) -> None:
        db.session.add(Customer(name='Spam', age=21))
        db.session.commit()

        self.assertEqual(None, cache.get('customers'))

        self.client.get(url_for('show_customers'))

        expected = [{'id': 1, 'age': 21, 'name': 'Spam'}]
        self.assertEqual(expected, cache.get('customers'))


class PostCustomersTest(BaseTest):
    def _post(self, data: Union[list, dict]) -> Response:
        return self.client.post(
            url_for('create_customers'),
            data=json.dumps(data),
            content_type='application/json',
        )

    def test_creates_customer(self) -> None:
        new_customers = [
            {'name': 'Meat', 'age': 32},
            {'name': 'Spam', 'age': 21},
        ]
        resp = self._post(new_customers)

        actual_customers = [{'name': customer.name, 'age': customer.age}
                            for customer in Customer.query.all()]
        self.assertEqual(HTTPStatus.CREATED, resp.status_code)
        self.assertEqual(new_customers, actual_customers)

    def test_show_error_if_post_data_is_empty_list(self) -> None:
        resp = self._post([])

        self.assertEqual(HTTPStatus.BAD_REQUEST, resp.status_code)
        self.assertEqual({'status': 'No donuts for you'}, resp.json)

    def test_clear_cache_if_customer_is_created(self) -> None:
        cache.set('customers', [{'id': 1, 'name': 'Spam', 'age': 99}])
        self._post([{'name': 'Meat', 'age': 32}])
        self.assertIsNone(cache.get('customers'))

    def test_dont_clear_cache_if_no_customer_is_ceated(self) -> None:
        cache.set('customers', [{'id': 1, 'name': 'Spam', 'age': 99}])
        self._post([])
        self.assertIsNotNone(cache.get('customers'))


class DeleteCustomersTest(BaseTest):
    def test_deletes_all_customers(self) -> None:
        db.session.add(Customer(name='Spam', age=33))
        db.session.add(Customer(name='Fish', age=11))
        db.session.commit()

        resp = self.client.delete(url_for('delete_customers'))

        self.assertEqual([], Customer.query.all())
        self.assertEqual(HTTPStatus.OK, resp.status_code)
        self.assertEqual({'status': 'Customers successfully deleted'},
                         resp.json)

    def test_clear_cache(self) -> None:
        cache.set('customers', [{'id': 1, 'name': 'Spam', 'age': 99}])

        self.client.delete(url_for('delete_customers'))

        self.assertIsNone(cache.get('customers'))

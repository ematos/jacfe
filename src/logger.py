import logging
from src import config

logger = logging.getLogger(config.APP_NAME)

if config.LOG_LEVEL:
    logger.setLevel(config.LOG_LEVEL)
    logger.addHandler(logging.StreamHandler())

from . import config
from .routes import app

if __name__ == '__main__':
    app.run(port=config.APP_PORT)

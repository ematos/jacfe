from typing import Hashable, Optional
import redis.exceptions
from flask_cache import Cache
from . import app, config
from .logger import logger


cache = Cache(app, config=config.CACHE_CONFIG)


def try_set(key: str, value: Hashable) -> None:
    try:
        cache.set(key, value)
    except redis.exceptions.ConnectionError:
        pass


def try_get(key: str) -> Optional[Hashable]:
    try:
        return cache.get(key)
    except redis.exceptions.ConnectionError:
        logger.warn('Cache server is down')
        return None


def try_clear() -> None:
    try:
        cache.clear()
    except redis.exceptions.ConnectionError:
        pass

from flask_sqlalchemy import SQLAlchemy

from . import app

db = SQLAlchemy(app)


class Customer(db.Model):  # type: ignore
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), nullable=False)
    age = db.Column(db.Integer, nullable=False)

    def __init__(self, name: str, age: int) -> None:
        self.name = name
        self.age = age

    def __repr__(self) -> str:
        return f'<Customer (name="{self.name}", age={self.age})>'

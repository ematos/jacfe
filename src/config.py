import logging
from os import environ

APP_NAME = 'jacfe'

LOG_LEVEL = {
    'critical': logging.CRITICAL,
    'error': logging.ERROR,
    'warn': logging.WARN,
    'info': logging.INFO,
    'debug': logging.DEBUG,
}.get(environ.get('JACFE_LOG_LEVEL', 'error'))

APP_PORT = int(environ.get('JACFE_PORT', 8080))

SQLALCHEMY_DATABASE_URI = environ.get('JACFE_DATABASE_URL', 'sqlite:///')
SQLALCHEMY_TRACK_MODIFICATIONS = False

CACHE_CONFIG = {
    'CACHE_TYPE': 'redis',
    'CACHE_REDIS_URL': environ.get('JACFE_CACHE_REDIS_URL',
                                   'redis://localhost'),
    'CACHE_ARGS': [],
}

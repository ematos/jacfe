from http import HTTPStatus
from typing import Union

from flask import jsonify, request, Response

from . import app, cache
from .logger import logger
from .models import Customer, db


@app.route('/customers', methods=['GET'])
def show_customers() -> Union[Response, str, tuple]:
    cached_customers = cache.try_get('customers')

    if cached_customers is not None:
        return jsonify(cached_customers)

    customers = [{
        'id': customer.id,
        'name': customer.name,
        'age': customer.age,
    } for customer in Customer.query.all()]
    cache.try_set('customers', customers)
    logger.info(f'Tried to regenerate cache with {len(customers)} customers')

    return jsonify(customers)


@app.route('/customers', methods=['POST'])
def create_customers() -> Union[Response, str, tuple]:
    new_customers = request.get_json()
    if not new_customers:
        return jsonify({'status': 'No donuts for you'}), HTTPStatus.BAD_REQUEST

    for new_customer in new_customers:
        db.session.add(Customer(**new_customer))
    db.session.commit()

    cache.try_clear()

    return (
        jsonify({'status': 'Customers successfully created'}),
        HTTPStatus.CREATED
    )


@app.route('/customers', methods=['DELETE'])
def delete_customers() -> Union[Response, str, tuple]:
    Customer.query.delete()
    db.session.commit()
    cache.try_clear()

    return jsonify({'status': 'Customers successfully deleted'})

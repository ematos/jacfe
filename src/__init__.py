from flask import Flask
from . import config


app = Flask(config.APP_NAME)
app.config.from_object(config)
